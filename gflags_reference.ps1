#
# configure which gflags version to build and package
#
$gflags_git_clone_url = "https://github.com/gflags/gflags.git"
$gflags_git_checkout = "v2.2.2"
