# GFlags Nuget

![Logo](res/logo.svg)

Scripting provided to build a nuget for gflags.
The default nuget is for the static lib only.
Feel free to use the AppVeyor artifacts if you like to pack something else.

* Source repository: https://github.com/gflags/gflags
* Project home page: https://go.grottel.net/nuget/gflags
* Package script repository: https://bitbucket.org/sgrottel_nuget/gflags_nuget

## Update

* Edit `.\gflags_reference.ps1` for an update of the source files.
* A push to the bitbucket repository will trigger Appveyor builds:
[![Build status](https://ci.appveyor.com/api/projects/status/kt4kxp5dsc48a42x?svg=true)](https://ci.appveyor.com/project/s_grottel/gflags-nuget)
[![Build status](https://ci.appveyor.com/api/projects/status/kt4kxp5dsc48a42x/branch/master?svg=true)](https://ci.appveyor.com/project/s_grottel/gflags-nuget/branch/master)
* As soon as the builds are all completed, run `.\collectArtifactos.ps1` do download the build artifacts for all targets to your local machine.
* Then, run `.\makeNuget.ps1` to pack the downloaded artifacts into a nupkg file.
* It is recommended to test that file with a small local project.
* If you are satisfied with the results, publish the nupkg to your nuget stream.
